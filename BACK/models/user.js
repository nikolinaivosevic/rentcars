export class User {
    constructor(
        korisnickoIme,
        lozinka,
        ime,
        prezime,
        pol,
        datumRodjenja,
        uloga,
        svaIznajmljivanja,
        korpa,
        objekat,
        brojBodova,
        tipKupca
    ){
        this.korisnickoIme = korisnickoIme;
        this.lozinka = lozinka;
        this.ime = ime;
        this.prezime = prezime;
        this.pol = pol;
        this.datumRodjenja = datumRodjenja;
        this.uloga = uloga;
        this.svaIznajmljivanja =svaIznajmljivanja;
        this.korpa = korpa;
        this.objekat = objekat;
        this.brojBodova = brojBodova;
        this.tipKupca = tipKupca;
    }
}