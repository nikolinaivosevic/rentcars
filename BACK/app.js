var http = require("http");
const fs = require("fs");
const dbUrl = "./db/db.json";
const url = require("url");

const rawReqToString = async (req) => {
  const buffers = [];
  for await (const chunk of req) {
    buffers.push(chunk);
  }
  return Buffer.concat(buffers).toString();
};

const sortByOpening = (a, b) => {
  // Converting to uppercase to have case-insensitive comparison
  const status1 = a.status.toUpperCase();
  const status2 = b.status.toUpperCase();

  let comparison = 0;

  if (status1 > status2) {
    comparison = 1;
  } else if (status1 < status2) {
    comparison = -1;
  }
  return comparison;
};

function checkOpeningTimes(openTime, closeTime) {
  let date = new Date();
  let hours = date.getHours() + ":" + date.getMinutes();
  if (hours.length == 4) {
    hours = "0" + hours;
  }
  return hours >= openTime && hours <= closeTime;
}
const month = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const langmap = {
  А: "A",
  Б: "B",
  В: "V",
  Г: "G",
  Д: "D",
  Ђ: "Đ",
  Е: "E",
  Ж: "Ž",
  З: "Z",
  И: "I",
  Ј: "J",
  К: "K",
  Л: "L",
  Љ: "Lj",
  М: "M",
  Н: "N",
  Њ: "Nj",
  О: "O",
  П: "P",
  Р: "R",
  С: "S",
  Т: "T",
  Ћ: "Ć",
  У: "U",
  Ф: "F",
  Х: "H",
  Ц: "C",
  Ч: "Č",
  Џ: "Dž",
  Ш: "Š",
  а: "a",
  б: "b",
  в: "v",
  г: "g",
  д: "d",
  ђ: "đ",
  е: "e",
  ж: "ž",
  з: "z",
  и: "i",
  ј: "j",
  к: "k",
  л: "l",
  љ: "lj",
  м: "m",
  н: "n",
  њ: "nj",
  о: "o",
  п: "p",
  р: "r",
  с: "s",
  т: "t",
  ћ: "ć",
  у: "u",
  ф: "f",
  х: "h",
  ц: "c",
  ч: "č",
  џ: "dž",
  ш: "š",
};

function remapLang(str) {
  return str.replace(/[^\u0000-\u007E]/g, function (a) {
    return langmap[a] || a;
  });
}

function checkRanking(brojBodova) {
  if (brojBodova > 4000) {
    return "Zlatan";
  }
  if (brojBodova > 3000) {
    return "Srebrni";
  }
  if (brojBodova > 2000) {
    return "Bronzani";
  } else {
    return "Obican";
  }
}

function ajdustRating(id) {
  fs.readFile(dbUrl, "utf8", function (err, data) {
    var obj = JSON.parse(data);
    let adjustRatingOfObject = [];
    for (let i = 0; i < obj.komentari.length; i++) {
      var element = obj.komentari[i];
      if (id == element.objekat && element.status == "Odobren") {
        adjustRatingOfObject.push(parseInt(element.ocena));
      }
    }
    for (let j = 0; j < obj.objects.length; j++) {
      var element2 = obj.objects[j];
      if (element2.id == id) {
        if (adjustRatingOfObject.length == 0) {
          element2.ocena = 0;
        } else {
          let ocena =
            adjustRatingOfObject.reduce((a, b) => a + b, 0) /
            adjustRatingOfObject.length;
          element2.ocena = ocena.toFixed(2);
        }
      }
    }
    var strData = JSON.stringify(obj);
    fs.writeFile(dbUrl, strData, (err) => {});
  });
}

async function reqListener(req, res) {
  const url = req.url;
  const method = req.method;
  const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "OPTIONS, GET, POST, PUT, PATCH, DELETE",
    "Access-Control-Allow-Headers": "Content-Type,Authorization,filename",
  };

  if (req.method === "OPTIONS") {
    res.writeHead(204, headers);
    res.end();
    return;
  }

  if (url === "/upload" && method === "POST") {
    res.writeHead(200, headers);
    const fileName = req.headers.filename;
    req.on("data", (chunk) => {
      fs.appendFileSync("./assets/logos/" + fileName, chunk);
    });
    req.on("end", () => {
      return res.end("uploaded!");
    });
  }

  if (url === "/" && method === "GET") {
    res.writeHead(200, headers);
    fs.readFile(dbUrl, "utf8", function (err, data) {
      let jsonDB = JSON.parse(data);
      let jsonObjs = jsonDB.objects;
      for (let i = 0; i < jsonObjs.length; i++) {
        var obj = jsonObjs[i];
        let open = checkOpeningTimes(
          obj.radnoVreme.pocetak,
          obj.radnoVreme.kraj
        );
        if (open) {
          obj.status = "Otvoreno";
        } else {
          obj.status = "Zatvoreno";
        }
      }
      let sorted = jsonObjs.sort(sortByOpening);
      return res.end(JSON.stringify(sorted));
    });
  }
  if (url === "/getObject" && method === "POST") {
    res.writeHead(200, headers);
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      let jsonDB = JSON.parse(data);
      let jsonObjs = jsonDB.objects;
      for (let i = 0; i < jsonObjs.length; i++) {
        var obj = jsonObjs[i];
        if (reqBody.id == obj.id) {
          let open = checkOpeningTimes(
            obj.radnoVreme.pocetak,
            obj.radnoVreme.kraj
          );
          if (open) {
            obj.status = "Otvoreno";
          } else {
            obj.status = "Zatvoreno";
          }
          let response = { msg: "SUCCESS", data: obj };
          return res.end(JSON.stringify(response));
        }
      }
      return res.end({ msg: "Not found" });
    });
  }

  if (url === "/newObject" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var obj = JSON.parse(data);
      let request = await fetch(
        "http://nominatim.openstreetmap.org/reverse?format=json&lon=" +
          reqBody.lokacija[0] +
          "&lat=" +
          reqBody.lokacija[1]
      );
      let result = await request.json();
      reqBody.address = result.address;
      reqBody.address.country = remapLang(result.address.country);
      reqBody.address.city = remapLang(result.address.city_district);
      obj.objects.push(reqBody);
      for (let i = 0; i < obj.users.length; i++) {
        const user = obj.users[i];
        if (reqBody.odabranMenadzer == user.id) {
          user.objekat = reqBody.id;
        }
      }
      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url === "/dodajVozilo" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var obj = JSON.parse(data);
      obj.vehicles.push(reqBody);
      for (let i = 0; i < obj.objects.length; i++) {
        const element = obj.objects[i];
        if (reqBody.objekat == element.id) {
          obj.objects[i].vozilaUponudi.push(reqBody.id);
        }
      }
      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url === "/dodajPorudzbinu" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var obj = JSON.parse(data);
      obj.porudzbine.push(reqBody);
      for (let i = 0; i < reqBody.vozila.length; i++) {
        const element = reqBody.vozila[i];
        for (let j = 0; j < obj.vehicles.length; j++) {
          var element2 = obj.vehicles[j];
          if (element.id == element2.id) {
            let data = element.dates;
            data.id = reqBody.id;
            element2.datumiNajma.push(data);
          }
        }
      }
      for (let k = 0; k < obj.users.length; k++) {
        var element3 = obj.users[k];
        if (element3.id == reqBody.idKupca) {
          let bodovi = (reqBody.cena / 1000) * 133;
          element3.brojBodova += bodovi;
          for (let l = 0; l < reqBody.objekti.length; l++) {
            const element4 = reqBody.objekti[l];
            if (!element3.canComment.includes(element4)) {
              element3.canComment.push(element4);
            }
          }
          element3.tipKupca = checkRanking(element3.brojBodova);
        }
      }
      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
      res.writeHead(200, headers);
      let response = {
        msg: "SUCCESS",
        data: reqBody,
        tipKupca: element3.tipKupca,
      };
      return res.end(JSON.stringify(response));
    });
  }

  if (url === "/porudzbine" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      let result = [];
      for (let i = 0; i < db.porudzbine.length; i++) {
        const element = db.porudzbine[i];
        if (element.objekti.includes(reqBody.id)) {
          result.push(element);
        }
      }
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: result };
      return res.end(JSON.stringify(response));
    });
  }

  if (url === "/mojePorudzbine" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      let result = [];
      for (let i = 0; i < db.porudzbine.length; i++) {
        const element = db.porudzbine[i];
        if (element.idKupca == reqBody.id) {
          result.push(element);
        }
      }
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: result };
      return res.end(JSON.stringify(response));
    });
  }

  if (url === "/promeniStatus" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      for (let i = 0; i < db.porudzbine.length; i++) {
        var element = db.porudzbine[i];
        if (element.id == reqBody.id) {
          element.status = reqBody.status;
          if (reqBody.razlogOdbijanja) {
            element.razlogOdbijanja = reqBody.razlogOdbijanja;
          }
        }
      }
      if (reqBody.status == "Vraceno") {
        for (let i = 0; i < reqBody.vozila.length; i++) {
          var element2 = reqBody.vozila[i];
          for (let j = 0; j < db.vehicles.length; j++) {
            var element3 = db.vehicles[j];
            if (element2.id == element3.id) {
              for (let k = 0; k < element3.datumiNajma.length; k++) {
                var element4 = element3.datumiNajma[k];
                console.log(element2.dates, element4);
                if (element2.dates.id == element4.id) {
                  element3.datumiNajma.splice(k, 1);
                }
              }
            }
          }
        }
      }
      var strData = JSON.stringify(db);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url === "/promeniStatusVozilima" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      for (let i = 0; i < db.vehicles.length; i++) {
        var element = db.vehicles[i];
        if (reqBody.ids.includes(element.id)) {
          element.status = reqBody.status;
        }
      }
      var strData = JSON.stringify(db);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url === "/otkaziPorudzbinu" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var obj = JSON.parse(data);
      for (let i = 0; i < obj.porudzbine.length; i++) {
        var element = obj.porudzbine[i];
        if (reqBody.id == element.id) {
          element.status = "Otkazana";
          for (let j = 0; j < obj.users.length; j++) {
            var element2 = obj.users[j];
            if (element2.id == reqBody.userId) {
              let pointsLost = (element.cena / 1000) * 133 * 4;
              element2.brojBodova -= pointsLost;
              element2.tipKupca = checkRanking(element2.brojBodova);
              const d = new Date();
              if (obj.currentMonth == month[d.getMonth()]) {
                element2.brojOtkazivanja += 1;
              } else {
                obj.currentMonth = month[d.getMonth()];
                element2.brojOtkazivanja = 1;
              }
            }
          }
        }
      }
      for (let i = 0; i < reqBody.vozila.length; i++) {
        var element2z = reqBody.vozila[i];
        for (let j = 0; j < obj.vehicles.length; j++) {
          var element3z = obj.vehicles[j];
          if (element2z.id == element3z.id) {
            for (let k = 0; k < element3z.datumiNajma.length; k++) {
              var element4z = element3z.datumiNajma[k];
              console.log(element2z.dates, element4z);
              if (element2z.dates.id == element4z.id) {
                element3z.datumiNajma.splice(k, 1);
              }
            }
          }
        }
      }

      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url === "/getVehicles" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      let vehicleList = [];
      for (let i = 0; i < reqBody.list.length; i++) {
        const element1 = reqBody.list[i];
        db.vehicles.forEach((element2) => {
          if (element2.id == element1) {
            vehicleList.push(element2);
          }
        });
      }
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: vehicleList };
      return res.end(JSON.stringify(response));
    });
  }

  if (url === "/getVehicle" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      let result;
      db.vehicles.forEach((element) => {
        if (element.id == reqBody.id) {
          result = element;
        }
      });
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: result };
      return res.end(JSON.stringify(response));
    });
  }

  if (url === "/proveriDostupnost" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      var pocetak = new Date(reqBody.pocetak).getTime();
      var kraj = new Date(reqBody.kraj).getTime();
      let result = [];
      for (let i = 0; i < db.vehicles.length; i++) {
        const element = db.vehicles[i];
        let isAvailable = true;

        if (element.datumiNajma.length == 0 && !result.includes(element)) {
          result.push(element);
        } else {
          for (let j = 0; j < element.datumiNajma.length; j++) {
            const element2 = element.datumiNajma[j];
            let pocetakNajma = new Date(element2.pocetak).getTime();
            let krajNajma = new Date(element2.kraj).getTime();
            if (
              (pocetakNajma <= pocetak && krajNajma <= pocetak) ||
              (pocetakNajma >= kraj && krajNajma >= kraj)
            ) {
            } else {
              isAvailable = false;
            }
          }
          if (isAvailable == true && !result.includes(element)) {
            result.push(element);
          }
        }
      }
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: result };
      return res.end(JSON.stringify(response));
    });
  }

  if (url === "/izmeniVozilo" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      let result;
      for (let i = 0; i < db.vehicles.length; i++) {
        var element = db.vehicles[i];
        if (element.id == reqBody.id) {
          db.vehicles[i] = reqBody;
        }
      }
      var strData = JSON.stringify(db);
      fs.writeFile(dbUrl, strData, (err) => {});
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: result };
      return res.end(JSON.stringify(response));
    });
  }
  if (url === "/obrisiVozilo" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", async function (err, data) {
      var db = JSON.parse(data);
      for (let i = 0; i < db.vehicles.length; i++) {
        var element = db.vehicles[i];
        if (element.id == reqBody.id) {
          db.vehicles.splice(i, 1);
          for (let j = 0; j < db.objects.length; j++) {
            var element2 = db.objects[j];
            if (element2.id == reqBody.objekat) {
              const index = element2.vozilaUponudi.indexOf(reqBody.id);
              if (index > -1) {
                element2.vozilaUponudi.splice(index, 1);
              }
            }
          }
        }
      }
      var strData = JSON.stringify(db);
      fs.writeFile(dbUrl, strData, (err) => {});
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: db.vehicles };
      return res.end(JSON.stringify(response));
    });
  }

  if (url === "/signup" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      obj.users.push(reqBody);
      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url == "/login" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    const { korisnickoIme, lozinka } = reqBody;
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var db = JSON.parse(data);
      for (let i = 0; i < db.users.length; i++) {
        const element = db.users[i];
        if (
          element.korisnickoIme == korisnickoIme &&
          element.lozinka == lozinka
        ) {
          res.writeHead(200, headers);
          let response = { msg: "SUCCESS", data: element };
          return res.end(JSON.stringify(response));
        }
      }
      res.writeHead(200, headers);
      let msg = { msg: "Wrong combination." };
      return res.end(JSON.stringify(msg));
    });
  }

  if (url === "/izmeni" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      let newArray = obj.users.filter((user) => user.id !== reqBody.id);
      newArray.push(reqBody);
      obj.users = newArray;
      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url === "/dodajKomentar" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      obj.komentari.push(reqBody);
      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url === "/changeCommentStatus" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      for (let i = 0; i < obj.komentari.length; i++) {
        var element = obj.komentari[i];
        if (reqBody.id == element.id) {
          element.status = reqBody.status;
        }
      }

      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {
        if (reqBody.status == "Odobren") {
          ajdustRating(reqBody.objectId);
        }
      });
    });
    res.writeHead(200, headers);
    let response = { msg: "SUCCESS", data: reqBody };
    return res.end(JSON.stringify(response));
  }

  if (url == "/getComments" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      let results = [];
      for (let i = 0; i < obj.komentari.length; i++) {
        const element = obj.komentari[i];
        if (element.objekat == reqBody.id) {
          results.push(element);
        }
      }
      res.writeHead(200, headers);
      return res.end(JSON.stringify({ results: results }));
    });
  }

  if (url == "/komentariNaCekanju" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      let results = [];
      for (let i = 0; i < obj.komentari.length; i++) {
        const element = obj.komentari[i];
        if (element.objekat == reqBody.id && element.status == "Na cekanju") {
          results.push(element);
        }
      }
      res.writeHead(200, headers);
      return res.end(JSON.stringify({ results: results }));
    });
  }

  if (url == "/getUsers" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      let results = [];
      for (let i = 0; i < obj.users.length; i++) {
        const user = obj.users[i];
        if (reqBody.sumnjivi) {
          if (user.brojOtkazivanja > 4) {
            results.push(user);
          }
        } else {
          if (user.uloga == "Kupac") {
            results.push(user);
          }
        }
      }
      res.writeHead(200, headers);
      return res.end(JSON.stringify({ data: results }));
    });
  }


  if (url == "/blockUser" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      for (let i = 0; i < obj.users.length; i++) {
        var user = obj.users[i];
        if (reqBody.id == user.id) {
          user.blokiran = reqBody.status;
        }
      }
      let newList = obj.users.filter(el=> el.uloga == "Kupac");
      var strData = JSON.stringify(obj);
      fs.writeFile(dbUrl, strData, (err) => {});
      res.writeHead(200, headers);
      let response = { msg: "SUCCESS", data: newList };
      return res.end(JSON.stringify(response));
    });
  }

  if (url == "/getFreeManagers" && method === "GET") {
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      let results = [];
      for (let i = 0; i < obj.users.length; i++) {
        const user = obj.users[i];
        if (user.uloga == "Menadzer" && user.objekat == "") {
          results.push({ id: user.id, ime: user.ime, prezime: user.prezime });
        }
      }
      res.writeHead(200, headers);
      return res.end(JSON.stringify({ results: results }));
    });
  }

  if (url.substring(0, 14) == "/assets/logos/" && method === "GET") {
    fs.readFile("." + url, (error, data) => {
      if (error) {
        res.writeHead(404);
        res.write("<h1>File Not Found</h1>");
      } else {
        res.writeHead(200);
        res.write(data);
      }
      res.end();
    });
  }
  if (url == "/search" && method === "POST") {
    let reqBody = JSON.parse(await rawReqToString(req));
    fs.readFile(dbUrl, "utf8", function (err, data) {
      var obj = JSON.parse(data);
      let resList = obj.objects;
      if (reqBody.lokacija) {
        let tempList;
        tempList = resList.filter((value) => {
          let city = value.address.city.toLowerCase();
          return city.includes(reqBody.lokacija.toLowerCase());
        });
        if (tempList !== resList) {
          resList = resList.filter((value) => {
            let city = value.address.country.toLowerCase();
            return city.includes(reqBody.lokacija.toLowerCase());
          });
        } else {
          resList = tempList;
        }
      }
      if (reqBody.naziv) {
        resList = resList.filter((value) => {
          let naziv = value.naziv.toLowerCase();
          return naziv.includes(reqBody.naziv.toLowerCase());
        });
      }
      if (reqBody.ocena) {
        resList = resList.filter((value) => {
          if (reqBody.ocena == value.ocena) {
            return true;
          }
        });
      }
      if (reqBody.tipVozila) {
        let tempList = [];
        for (let i = 0; i < obj.vehicles.length; i++) {
          const element = obj.vehicles[i];
          let tipVozila = element.tip.toLowerCase();
          if (tipVozila.includes(reqBody.tipVozila.toLowerCase())) {
            tempList = resList.filter((value) => {
              if (value.id == element.objekat) {
                return true;
              }
            });
          }
        }
        if (tempList.length == 0) {
          resList = [];
        } else {
          resList = tempList;
        }
      }
      if (reqBody.vrstaVozila) {
        for (let i = 0; i < obj.vehicles.length; i++) {
          const element = obj.vehicles[i];
          if (element.vrsta == reqBody.vrstaVozila) {
            resList = resList.filter((value) => {
              if (value.id == element.objekat) {
                return true;
              }
            });
          }
        }
      }
      if (reqBody.tipGoriva) {
        let tempList = [];
        for (let i = 0; i < obj.vehicles.length; i++) {
          const element = obj.vehicles[i];
          if (element.tipGoriva == reqBody.tipGoriva) {
            tempList = resList.filter((value) => {
              if (value.id == element.objekat) {
                return true;
              }
            });
          }
        }
        if (tempList.length == 0) {
          resList = [];
        } else {
          resList = tempList;
        }
      }
      for (let i = 0; i < resList.length; i++) {
        var obj = resList[i];
        let open = checkOpeningTimes(
          obj.radnoVreme.pocetak,
          obj.radnoVreme.kraj
        );
        if (open) {
          obj.status = "Otvoreno";
        } else {
          obj.status = "Zatvoreno";
        }
      }
      if (reqBody.samoOtvoreni) {
        resList = resList.filter((value) => {
          if (value.status == "Otvoreno") {
            return true;
          }
        });
      }
      if (reqBody.sortiranje) {
        if (reqBody.sortiranje == "nazivRastuce") {
          resList.sort((a, b) => a.naziv.localeCompare(b.naziv));
        }
        if (reqBody.sortiranje == "nazivOpadajuce") {
          resList.sort((a, b) => b.naziv.localeCompare(a.naziv));
        }
        if (reqBody.sortiranje == "lokacijaRastuce") {
          resList.sort((a, b) => a.address.city.localeCompare(b.address.city));
        }
        if (reqBody.sortiranje == "lokacijaOpadajuce") {
          resList.sort((a, b) => b.address.city.localeCompare(a.address.city));
        }
        if (reqBody.sortiranje == "ocenaRastuce") {
          resList.sort((a, b) => a.ocena - b.ocena);
        }
        if (reqBody.sortiranje == "ocenaOpadajuce") {
          resList.sort((a, b) => b.ocena - a.ocena);
        }
      } else {
        resList = resList.sort(sortByOpening);
      }
      res.writeHead(200, headers);
      res.end(JSON.stringify(resList));
    });
  }
}

const server = http.createServer(reqListener);
server.listen(3000);
