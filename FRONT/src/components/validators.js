export function validateIme(input) {
  var len = input.length;
  if (len == 0) {
    return {
      message: "Ime je potrebno.",
    };
  } else if (len < 5 || len > 20) {
    return {
      message: "Ime mora biti izmedju 5 i 20 karaktera.",
    };
  } else {
    return {};
  }
}
export function validatePrezime(input) {
  var len = input.length;
  if (len == 0) {
    return {
      message: "Prezime je potrebno.",
    };
  } else if (len < 5 || len > 20) {
    return {
      message: "Prezime mora biti izmedju 5 i 20 karaktera.",
    };
  } else {
    return {};
  }
}
export function validateUsername(input) {
  var len = input.length;
  if (len == 0) {
    return {
      message: "Korisnicko ime je potrebno.",
    };
  } else if (len < 5 || len > 20) {
    return {
      message: "Korisnicko ime mora biti izmedju 5 i 20 karaktera.",
    };
  } else {
    return {};
  }
}
export function validatePassword(input) {
  var len = input.length;
  if (len == 0) {
    return {
      message: "Lozinka je potrebna",
    };
  } else if (len < 5 || len > 20) {
    return {
      message: "Password must be between 5 to 20 characters long.",
    };
  } else {
    return {};
  }
}
