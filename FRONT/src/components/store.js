import Vue from "vue";

export const store = Vue.observable({
  data: {
    isLoggedIn: false,
    user: {},
  },
});
