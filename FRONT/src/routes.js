import Pocetna from "./views/Pocetna.vue";
import Prijava from "./views/Prijava.vue";
import Registracija from "./views/registracija.vue";
import Profil from "./views/profil.vue";
import DodajObjekat from "./views/dodajobjekat.vue";
import Objekat from "./views/objekat.vue";
import DodajMenadzera from "./views/dodajmenadzera.vue";
import IzmeniProfil from "./views/izmeniprofil.vue";
import DodajVozilo from "./views/dodajVozilo.vue";
import IzmenaVozila from "./views/izmenaVozila.vue";
import IzmeniVozilo from "./views/izmeniVozilo.vue";
import IznajmiVozilo from "./views/iznajmivozilo.vue";
import MojaKorpa from "./views/mojaKorpa.vue";
import MenPorudzbine from "./views/menPorudzbine.vue";
import MojePorudzbine from "./views/mojePorudzbine.vue";
import ProveraKomentara from "./views/ProveraKomentara.vue";
import ProveraKorisnika from "./views/ProveraKorisnika.vue";
export default [
  {
    path: "/",
    name: "pocetna",
    component: Pocetna,
  },
  {
    path: "/prijava",
    name: "prijava",
    component: Prijava,
  },
  {
    path: "/registracija",
    name: "registracija",
    component: Registracija,
  },
  {
    path: "/profil",
    name: "profil",
    component: Profil,
  },
  {
    path: "/dodajObj",
    name: "dodajObj",
    component: DodajObjekat,
  },
  {
    path: "/objekat/:id",
    component: Objekat,
  },
  {
    path: "/dodajMen",
    name: "dodajMen",
    component: DodajMenadzera,
  },
  {
    path: "/izmeniProfil",
    name: "izmeniProfil",
    component: IzmeniProfil,
  },
  {
    path: "/dodajVozilo",
    name: "dodajVozilo",
    component: DodajVozilo,
  },
  {
    path: "/izmenaVozila",
    name: "izmenaVozila",
    component: IzmenaVozila,
  },
  {
    path: "/izmeniVozilo/:id",
    name: "izmeniVozilo",
    component: IzmeniVozilo,
  },
  {
    path: "/iznajmiVozilo",
    name: "iznajmiVozilo",
    component: IznajmiVozilo,
  },
  {
    path: "/mojaKorpa",
    name: "mojaKorpa",
    component: MojaKorpa,
  },
  {
    path: "/menPorudzbine",
    name: "menPorudzbine",
    component: MenPorudzbine,
  },
  {
    path: "/mojePorudzbine",
    name: "mojePorudzbine",
    component: MojePorudzbine,
  },
  {
    path: "/proveraKomentara",
    name: "proveraKomentara",
    component: ProveraKomentara,
  },
  {
    path: "/proveraKorisnika",
    name: "proveraKorisnika",
    component: ProveraKorisnika,
  },
];
