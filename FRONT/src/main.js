import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Routes from './routes'
import vTostini from 'v-tostini'

Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(vTostini)

const router = new VueRouter({
  routes:Routes,
  mode:'history',
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact-active-link',
})

new Vue({
  el: '#app',
  render: h => h(App),
  router:router
})
